// 階乗関数の本体
int fact(int x) {
    int ans = -1; // 負の時用に -1 を準備
    if (x > -1) {
        for (ans = 1; x > 0; x--) { // 0以上なら ans の初期値を 1 にする
            ans *= x;
        }
    }
    return ans;
}

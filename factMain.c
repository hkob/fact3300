#include <stdio.h>
#include <stdlib.h>
#include "fact.h"

int main() {
    int x, ans;
    fprintf(stderr, "整数を入力して下さい:");
    scanf("%d", &x);
    ans = fact(x);
    if (ans < 0) {
        printf("負の値の階乗は計算できません\n");
    } else {
        printf("%d の階乗は %d です．\n", x, ans);
    }
    return 0;
}


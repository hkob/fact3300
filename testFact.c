#include <stdio.h>
#include "fact.h"
#include "testCommon.h"

int main() {
    assertEqualsInt(fact(1), 1); // 1 の階乗が 1 かどうかテスト
    assertEqualsInt(fact(2), 2); // 2 の階乗が 2 かどうかテスト
    assertEqualsInt(fact(3), 6); // 3 の階乗が 6 かどうかテスト
    assertEqualsInt(fact(6), 720); // 6 の階乗が 720 かどうかテスト
    assertEqualsInt(fact(0), 1); // 0 の階乗が 1 かどうかテスト
    assertEqualsInt(fact(-1), -1); // -1 の階乗はエラーとして-1を返す
    assertEqualsInt(fact(-5), -1); // -5 の階乗もエラーとして-1を返す
    testErrorCheck();
    return 0;
}
